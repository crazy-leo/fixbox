module.exports = api => {
  api.cache(true);

  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "module-resolver",
        {
          root: ["./src"],
          alias: {
            "@@assets": "./assets",
            "@env": "./env.js"
          }
        }
      ]
    ],
    env: {
      development: {
        plugins: ["@babel/plugin-transform-react-jsx-source"]
      }
    }
  };
};
