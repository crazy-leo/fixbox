/* eslint-disable no-shadow */
export const desctructFields = fields => {
  const desctructedFields = {};

  fields.forEach(({ name, value }) => {
    desctructedFields[name] = value;
  });

  return desctructedFields;
};

export const createSummaryFields = (fields, name, value) =>
  fields
    .filter(field => field.name !== name)
    .map(({ name, value }) => ({ name, value }))
    .concat({ name, value });
