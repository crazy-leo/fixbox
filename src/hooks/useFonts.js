import * as React from "react";
import * as Font from "expo-font";

export default () => {
  const [loaded, load] = React.useState(false);

  React.useEffect(() => {
    Font.loadAsync({
      "lato-black": require("../../assets/fonts/Lato-Black.ttf"),
      "lato-bold": require("../../assets/fonts/Lato-Bold.ttf"),
      "lato-hairline": require("../../assets/fonts/Lato-Hairline.ttf"),
      "lato-heavy": require("../../assets/fonts/Lato-Heavy.ttf"),
      "lato-italic": require("../../assets/fonts/Lato-Italic.ttf"),
      "lato-light": require("../../assets/fonts/Lato-Light.ttf"),
      "lato-medium": require("../../assets/fonts/Lato-Medium.ttf"),
      "lato-regular": require("../../assets/fonts/Lato-Regular.ttf"),
      "lato-semibold": require("../../assets/fonts/Lato-Semibold.ttf"),
      "lato-thin": require("../../assets/fonts/Lato-Thin.ttf")
    }).then(() => load(true));
  }, []);

  return loaded;
};
