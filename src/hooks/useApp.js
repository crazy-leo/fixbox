/* eslint-disable react-hooks/exhaustive-deps, no-shadow */
import * as React from "react";
import { AsyncStorage } from "react-native";
import { SplashScreen } from "expo";
import useFonts from "./useFonts";

export default () => {
  const loaded = useFonts();
  const [{ token, isAppReady }, setState] = React.useState({
    token: "",
    isAppReady: false
  });

  const login = React.useCallback(
    token => setState({ token, isAppReady: true }),
    [setState]
  );

  React.useEffect(() => {
    (async () => {
      SplashScreen.preventAutoHide();

      if (!token && !isAppReady && loaded) {
        const token = await AsyncStorage.getItem("@auth-token");

        setState({
          token: token || "",
          isAppReady: true
        });

        SplashScreen.hide();
      }
    })();
  }, [loaded]);

  return {
    token,
    isAppReady,
    login
  };
};
