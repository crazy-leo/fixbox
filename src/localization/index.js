import * as Localization from "expo-localization";
import i18n from "i18n-js";
import ru from "./ru";

i18n.fallbacks = true;
i18n.translations = { ru };
i18n.locale = Localization.locale;
i18n.defaultLocale = "ru-RU";

export default i18n;
