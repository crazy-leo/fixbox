export default {
  error: "Ошибка",
  nameQuestion: "Как вас зовут?",
  nameQuestionHint: "Полное имя, например: Иван Иванов",
  createNickname: "Придумайте ник",
  createNicknameHint: "Например: ivan_ivanov",
  yourNickname: "Ваш ник",
  yourPassword: "Ваш пароль",
  createPassword: "Придумайте пароль",
  invalidFullname: "Некорректное имя",
  usernameExists: "Такое имя пользователя уже существует",
  hasAccountQuestion: "У вас уже есть аккаунт?",
  hasNotAccountQuestion: "У вас нет аккаунта?",
  signup: "Зарегистрируйтесь",
  login: "Вход",
  usernameNotExists: "Такое имя пользователя не существует",
  passwordNotExists: "Неправильный пароль",
  profile: "Профиль",
  myCourses: "Мои курсы",
  myBoxes: "Мои боксы",
  winners: "Победители",
  cooperation: "Сотрудничество",
  help: "Помощь",
  invalidUsername:
    "Вы должны использовать только латинские буквы, цифры и символы: '_', '-', '.'. Минимальная длина - 3 символа. Максимальная длина - 30 символов.",
  invalidPassword:
    "Вы должны использовать только латинские буквы, цифры и символы: '_', '-', '.'. Минимальная длина - 3 символа. Максимальная длина - 30 символов."
};
