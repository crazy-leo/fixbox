/* eslint-disable no-shadow */
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import axios from "axios";
import axiosMiddleware from "redux-axios-middleware";
import reducer from "ducks";
import env from "@env";

const api = axios.create({
  baseURL: env.apiUrl,
  responseType: "json"
});

const store = createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(
      axiosMiddleware(api, {
        onError({ error }) {
          const normalizedError = error.response ? error.response.data : error;
          return Promise.reject(normalizedError);
        }
      })
    )
  )
);

export default store;
