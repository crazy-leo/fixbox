import { theme } from "galio-framework";
import { DefaultTheme } from "@react-navigation/native";

export default () => {
  return {
    ...theme,
    COLORS: {
      ...theme.COLORS,
      PRIMARY: "#4978e3",
      BACKGROUND: "#ffffff",
      /* Only for galio framework */
      THEME: "#4978e3"
    }
  };
};

export const getNavigationTheme = () => {
  return {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors
    }
  };
};
