import * as React from "react";
import PropTypes from "prop-types";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import SignUpScreen, { options as signUpOptions } from "components/SignUp";
import SignInScreen, { options as signInOptions } from "components/SignIn";
import ProfileScreen, { options as profileOptions } from "components/Profile";

const Stack = createStackNavigator();
const Navigator = ({ token, theme }) => (
  <NavigationContainer theme={theme}>
    <Stack.Navigator>
      {token ? (
        <Stack.Screen
          name="Profile"
          component={ProfileScreen}
          options={profileOptions}
        />
      ) : (
        <>
          <Stack.Screen
            name="SignIn"
            component={SignInScreen}
            options={signInOptions}
          />
          <Stack.Screen
            name="SignUp"
            component={SignUpScreen}
            options={signUpOptions}
          />
        </>
      )}
    </Stack.Navigator>
  </NavigationContainer>
);

Navigator.propTypes = {
  token: PropTypes.string.isRequired,
  theme: PropTypes.object.isRequired
};

export default Navigator;
