import { combineReducers } from "redux";
import signup from "components/SignUp/duck";
import signin from "components/SignIn/duck";

export const ducks = { signup, signin };
export default combineReducers({
  [signup.store]: signup.reducer,
  [signin.store]: signin.reducer
});
