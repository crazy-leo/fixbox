/* eslint-disable no-useless-escape */
import * as Yup from "yup";
import localization from "localization";

const usernameRegExp = /^[a-z0-9\._-]{3,30}$/i;
const passwordRegExp = usernameRegExp;

export const FullnameScheme = Yup.string()
  .required(localization.t("invalidFullname"))
  .min(3, localization.t("invalidFullname"))
  .max(40, localization.t("invalidFullname"));

export const UsernameScheme = Yup.string()
  .required(localization.t("invalidUsername"))
  .matches(usernameRegExp, localization.t("invalidUsername"))
  .max(30, localization.t("invalidUsername"));

export const PasswordScheme = Yup.string()
  .required(localization.t("invalidPassword"))
  .matches(passwordRegExp, localization.t("invalidPassword"))
  .max(30, localization.t("invalidPassword"));

export const SignUpScheme = Yup.object().shape({
  fullname: FullnameScheme,
  username: UsernameScheme,
  password: PasswordScheme
});

export const SignInScheme = Yup.object().shape({
  username: UsernameScheme,
  password: PasswordScheme
});

export { Yup };
