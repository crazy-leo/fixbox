import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ducks } from "ducks";

export default getState => (component, name) => {
  const chunk = ducks[name];
  const mapState = state => getState(state, chunk.selectors, state[name]);
  const mapDispatch = dispatch => ({
    actions: bindActionCreators(chunk.creators, dispatch)
  });

  return connect(mapState, mapDispatch)(component);
};
