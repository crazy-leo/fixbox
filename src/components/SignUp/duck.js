import Duck from "extensible-duck";
import { createSelector } from "reselect";
import localization from "localization";
import { desctructFields } from "services/form";

export default new Duck({
  namespace: "fixbox",
  store: "signup",

  // Actions types
  types: [
    "UPDATE_INPUT",
    "SET_ERROR",
    "UPDATE_STEP",
    "CHECK_USERNAME",
    "CREATE_USER",
    "LOADING"
  ],

  // Initial state
  initialState: {
    steps: 3,
    step: 1,
    token: "",
    loading: false,
    nextStep: false,
    error: "",
    inputs: [
      {
        value: "",
        step: 1,
        name: "fullname",
        placeholder: localization.t("nameQuestion"),
        hint: localization.t("nameQuestionHint"),
        error: ""
      },
      {
        value: "",
        step: 2,
        name: "username",
        placeholder: localization.t("createNickname"),
        hint: localization.t("createNicknameHint"),
        autoCapitalize: "none",
        error: ""
      },
      {
        value: "",
        step: 3,
        name: "password",
        placeholder: localization.t("createPassword"),
        hint: "",
        autoCapitalize: "none",
        error: ""
      }
    ]
  },

  // Selectors
  selectors: {
    isFinishStep: createSelector(
      state => state.steps,
      state => state.step,
      (steps, step) => step >= steps
    )
  },

  // Actions
  creators: duck => ({
    updateField: props => ({ type: duck.types.UPDATE_INPUT, payload: props }),
    updateStep: step => ({ type: duck.types.UPDATE_STEP, payload: step }),

    checkUsername: username => ({
      type: duck.types.CHECK_USERNAME,
      payload: {
        request: {
          url: `/public/check-username/${username}`
        }
      }
    }),

    toggleLoading: state => ({ type: duck.types.LOADING, payload: state }),

    createUser: fields => ({
      type: duck.types.CREATE_USER,
      payload: {
        request: {
          url: `/public/create-user`,
          method: "POST",
          data: desctructFields(fields)
        }
      }
    })
  }),

  // reducer
  reducer: (state, action, duck) => {
    switch (action.type) {
      case duck.types.LOADING:
        return {
          ...state,
          loading: action.payload
        };

      case duck.types.UPDATE_INPUT:
        return {
          ...state,
          nextStep: state.step + 1,
          inputs: state.inputs.map(input =>
            input.name === action.payload.name
              ? { ...input, ...action.payload }
              : input
          )
        };

      case duck.types.UPDATE_STEP:
        return {
          ...state,
          step: action.payload,
          nextStep: false
        };

      case duck.types.CHECK_USERNAME:
      case duck.types.CREATE_USER:
        return {
          ...state,
          loading: true
        };

      case `${duck.types.CHECK_USERNAME}_SUCCESS`:
      case `${duck.types.CHECK_USERNAME}_FAIL`:
      case `${duck.types.CREATE_USER}_FAIL`:
        return {
          ...state,
          loading: false
        };

      case `${duck.types.CREATE_USER}_SUCCESS`:
        return {
          ...state,
          loading: false,
          token: action.payload.data.token
        };

      default:
        return state;
    }
  }
});
