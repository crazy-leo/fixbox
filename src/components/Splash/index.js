import React from "react";
import Block from "components/common/Block";
import { Image } from "react-native";
import splash from "@@assets/splash.png";

export default () => (
  <Block flex>
    <Image source={splash} />
  </Block>
);
