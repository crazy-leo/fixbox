import { ActivityIndicator } from "react-native";
import styled from "styled-components/native";

export default styled(ActivityIndicator)`
  opacity: ${props => (props.enable ? 1 : 0)};
`;
