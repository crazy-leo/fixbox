import * as React from "react";
import PropTypes from "prop-types";
import { useNavigation } from "@react-navigation/native";
import Wrapper from "components/common/Wrapper";
import Input from "./Input";
import Header from "./Header";
import Footer from "./Footer";

const AuthenticationStep = ({ type, field, onBack, step, loading, onNext }) => {
  const onSubmit = React.useCallback(
    event => {
      onNext(field.name, event.nativeEvent.text);
    },
    [field.name, onNext]
  );

  const navigation = useNavigation();
  const onSpecificStep = React.useCallback(() => {
    navigation.navigate(type === "signin" ? "SignUp" : "SignIn");
  }, [navigation, type]);

  return (
    <Wrapper withStatusBarHeight>
      <Header loading={loading} step={step - 1} onBack={onBack} />
      <Input {...field} onSubmit={onSubmit} autoFocus />
      <Footer type={type} onPress={onSpecificStep} />
    </Wrapper>
  );
};

AuthenticationStep.propTypes = {
  type: PropTypes.string.isRequired,
  field: PropTypes.shape({
    name: PropTypes.string.isRequired
  }).isRequired,
  onNext: PropTypes.func.isRequired,
  onBack: PropTypes.func.isRequired,
  step: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired
};

export default AuthenticationStep;
