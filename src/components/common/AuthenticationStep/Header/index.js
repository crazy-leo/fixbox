import * as React from "react";
import PropTypes from "prop-types";
import { withGalio } from "galio-framework";
import Icon from "components/common/Icon";
import Block from "components/common/Block";
import { StyledIndicator } from "./styled";

const Header = ({ theme, onBack, step, loading }) => (
  <Block row>
    {!step ? null : (
      <Icon
        name="arrowleft"
        family="AntDesign"
        color={theme.COLORS.PRIMARY}
        size={30}
        onPress={onBack}
      />
    )}
    <StyledIndicator enable={loading} />
  </Block>
);

Header.propTypes = {
  theme: PropTypes.objectOf(PropTypes.object.isRequired).isRequired,
  onBack: PropTypes.func.isRequired,
  step: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired
};

export default withGalio(Header);
