import styled from "styled-components/native";
import NativeIndicator from "components/common/NativeIndicator";

export const StyledIndicator = styled(NativeIndicator)`
  margin: auto 0 auto auto;
`;
