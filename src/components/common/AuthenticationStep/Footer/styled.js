import styled from "styled-components/native";
import Block from "components/common/Block";
import Text from "components/common/Text";

export const StyledFooter = styled(Block)`
  margin-top: auto;
  margin-bottom: 25px;
`;

export const StyledText = styled(Text)`
  text-align: center;
  font: 15px lato-light;
  color: ${props => props.theme.COLORS.MUTED};
`;

export const StyledNavigationText = styled(StyledText)`
  font-family: lato-bold;
  margin-left: 10px;
  font-size: 17px;
  color: ${props => props.theme.COLORS.PRIMARY};
`;
