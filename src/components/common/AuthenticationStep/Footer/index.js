import React from "react";
import PropTypes from "prop-types";
import localization from "localization";
import { StyledFooter, StyledText, StyledNavigationText } from "./styled";

const Footer = ({ type, onPress }) => {
  return (
    <StyledFooter row center>
      <StyledText>
        {localization.t(
          type === "signin" ? "hasNotAccountQuestion" : "hasAccountQuestion"
        )}
      </StyledText>
      <StyledNavigationText onPress={onPress}>
        {localization.t(type === "signin" ? "signup" : "login")}
      </StyledNavigationText>
    </StyledFooter>
  );
};

Footer.propTypes = {
  type: PropTypes.oneOf(["signin", "signup"]).isRequired,
  onPress: PropTypes.func.isRequired
};

export default Footer;
