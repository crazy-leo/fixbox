import styled from "styled-components/native";
import Block from "components/common/Block";
import Text from "components/common/Text";

export const StyledWrapper = styled(Block)`
  position: relative;
  margin-top: auto;
  margin-bottom: ${props => props.bottomPadding}px;
`;

export const StyledHint = styled(Text)`
  margin-right: auto;
  font: 14px lato-light;
  color: ${props => props.theme.COLORS.BLACK};
`;
