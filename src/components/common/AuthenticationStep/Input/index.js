/* eslint-disable no-shadow */
import * as React from "react";
import PropTypes from "prop-types";
import { getStatusBarHeight } from "@freakycoder/react-native-helpers";
import { withGalio } from "galio-framework";
import { Akira } from "react-native-textinput-effects";
import { StyledWrapper, StyledHint } from "./styled";
import styles from "./styles";

const Input = ({ styles, name, onSubmit, placeholder, hint, ...props }) => {
  const statusBarHeightheight = getStatusBarHeight();

  return (
    <StyledWrapper bottomPadding={statusBarHeightheight}>
      <Akira
        label={placeholder}
        borderColor="transparent"
        height={30}
        maxLength={50}
        style={styles.container}
        labelStyle={styles.label}
        inputStyle={styles.input}
        onSubmitEditing={onSubmit}
        inputPadding={0}
        autoCompleteType="off"
        returnKeyType="go"
        autoCorrect={false}
        enablesReturnKeyAutomatically
        {...props}
      />
      <StyledHint>{hint}</StyledHint>
    </StyledWrapper>
  );
};

Input.defaultProps = {
  hint: ""
};

Input.propTypes = {
  styles: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  hint: PropTypes.string,
  value: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default withGalio(Input, styles);
