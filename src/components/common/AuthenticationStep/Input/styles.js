import { StyleSheet } from "react-native";

export default theme =>
  StyleSheet.create({
    container: {
      width: "100%"
    },
    label: {
      color: theme.COLORS.BLACK,
      fontFamily: "lato-light",
      fontSize: 26,
      height: 30,
      textAlign: "left"
    },
    input: {
      color: theme.COLORS.BLACK,
      textAlign: "left",
      fontFamily: "lato-regular",
      paddingLeft: 0
    }
  });
