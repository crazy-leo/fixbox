import styled from "styled-components/native";
import { KeyboardAvoidingView, View, Platform } from "react-native";

export const StyledWrapper = styled(
  Platform.OS === "ios" ? KeyboardAvoidingView : View
)`
  background-color: ${props => props.theme.COLORS.BACKGROUND};
  padding: ${props => (props.withStatusBarHeight ? props.paddingTop : 20)}px
    15px 20px 15px;
`;
