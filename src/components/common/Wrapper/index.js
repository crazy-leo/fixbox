import React from "react";
import PropTypes from "prop-types";
import { getStatusBarHeight } from "@freakycoder/react-native-helpers";
import { StyledWrapper } from "./styled";

const Wrapper = ({ children, withStatusBarHeight }) => {
  return (
    <StyledWrapper
      flex
      withStatusBarHeight={withStatusBarHeight}
      paddingTop={withStatusBarHeight && getStatusBarHeight()}
      behavior="padding"
    >
      {children}
    </StyledWrapper>
  );
};

Wrapper.defaultProps = {
  withStatusBarHeight: false
};

Wrapper.propTypes = {
  withStatusBarHeight: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default Wrapper;
