import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity } from "react-native";
import { withGalio } from "galio-framework";
import { getStatusBarHeight } from "@freakycoder/react-native-helpers";
import Panel from "./Panel";
import {
  StyledWrapper,
  StyledArrow,
  StyledTitle,
  StyledText,
  StyledButton
} from "./styled";

const Header = ({ theme }) => {
  const statusBarHeight = getStatusBarHeight();
  const [isOpenPanel, togglePanel] = React.useState(false);

  const togglePanelHandle = React.useCallback(
    isOpen => () => togglePanel(isOpen),
    []
  );

  return (
    <>
      <StyledWrapper row paddingTop={statusBarHeight}>
        <StyledTitle row onPress={togglePanelHandle(true)}>
          <StyledText>Профиль</StyledText>
          <StyledArrow
            name="ios-arrow-down"
            family="ionicon"
            color={theme.COLORS.PRIMARY}
            size={20}
          />
        </StyledTitle>
        <TouchableOpacity onPress={togglePanelHandle(true)}>
          <StyledButton
            name="md-menu"
            family="ionicon"
            color={theme.COLORS.PRIMARY}
            size={35}
          />
        </TouchableOpacity>
      </StyledWrapper>
      <Panel isOpen={isOpenPanel} onClose={togglePanelHandle(false)} />
    </>
  );
};

Header.propTypes = {
  theme: PropTypes.object.isRequired
};

export default withGalio(Header);
