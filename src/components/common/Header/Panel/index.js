import React from "react";
import PropTypes from "prop-types";
import localization from "localization";
import SwipeablePanel from "rn-swipeable-panel";
import Item from "./Item";

const Panel = ({ isOpen, onClose }) => {
  const onPress = React.useCallback(() => {
    console.log("Hello world");
  }, []);

  return (
    <SwipeablePanel closeOnTouchOutside isActive={isOpen} onClose={onClose}>
      <Item
        iconName="face-profile"
        iconFamily="material-community"
        data-screen-name="profile"
        text={localization.t("profile")}
        onPress={onPress}
      />
      <Item
        iconName="folder-video"
        iconFamily="entypo"
        data-screen-name="my-courses"
        text={localization.t("myCourses")}
        onPress={onPress}
      />
      <Item
        iconName="dropbox"
        iconFamily="entypo"
        data-screen-name="my-boxes"
        text={localization.t("myBoxes")}
        onPress={onPress}
      />
      <Item
        iconName="ios-people"
        iconFamily="ionicon"
        data-screen-name="winners"
        text={localization.t("winners")}
        onPress={onPress}
      />
      <Item
        iconName="praying-hands"
        iconFamily="font-awesome-5"
        data-screen-name="cooperation"
        text={localization.t("cooperation")}
        onPress={onPress}
        smallIcon
      />
      <Item
        iconName="help"
        iconFamily="entypo"
        data-screen-name="help"
        text={localization.t("help")}
        onPress={onPress}
      />
    </SwipeablePanel>
  );
};

Panel.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

export default Panel;
