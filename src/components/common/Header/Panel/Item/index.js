import React from "react";
import PropTypes from "prop-types";
import { withGalio } from "galio-framework";
import { StyledItem, StyledText, StyledIcon } from "./styled";

const Item = ({ iconName, iconFamily, text, theme, onPress }) => (
  <StyledItem row onPress={onPress}>
    <StyledIcon
      name={iconName}
      color={theme.COLORS.PRIMARY}
      family={iconFamily}
      size={25}
    />
    <StyledText>{text}</StyledText>
  </StyledItem>
);

Item.propTypes = {
  iconName: PropTypes.string.isRequired,
  iconFamily: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  theme: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired
};

export default withGalio(Item);
