import styled from "styled-components/native";
import Text from "components/common/Text";
import Icon from "components/common/Icon";
import Block from "components/common/Block";

export const StyledItem = styled(Block)`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 14px 20px;
  border-bottom-width: 1px;
  border-bottom-color: ${props => props.theme.COLORS.NAVBAR};
`;

export const StyledIcon = styled(Icon)`
  min-width: 50px;
`;

export const StyledText = styled(Text)`
  font-size: 18px;
  font-family: lato-medium;
`;
