import styled from "styled-components/native";
import Block from "components/common/Block";
import { TouchableOpacity } from "react-native";
import Text from "components/common/Text";
import Icon from "components/common/Icon";

export const StyledWrapper = styled(Block)`
  position: relative;
  padding-top: ${props => props.paddingTop}px;
`;

export const StyledTitle = styled(TouchableOpacity)`
  padding: 10px;
  font-size: 18px;
  flex: 1;
  flex-direction: row;
`;

export const StyledText = styled(Text)`
  font-size: 18px;
  margin-right: 10px;
`;

export const StyledArrow = styled(Icon)`
  margin-top: 2px;
`;

export const StyledButton = styled(Icon)`
  position: absolute;
  right: 5px;
  bottom: -3px;
  padding: 10px 10px 5px 10px;
  text-decoration-color: red;
`;
