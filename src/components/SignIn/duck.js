import Duck from "extensible-duck";
import { createSelector } from "reselect";
import localization from "localization";
import { desctructFields } from "services/form";

export default new Duck({
  namespace: "fixbox",
  store: "signin",

  // Actions types
  types: ["UPDATE_INPUT", "SET_ERROR", "UPDATE_STEP", "LOADING", "SIGNIN"],

  // Initial state
  initialState: {
    steps: 2,
    step: 1,
    token: "",
    loading: false,
    nextStep: false,
    error: "",
    inputs: [
      {
        value: "",
        step: 1,
        name: "username",
        placeholder: localization.t("yourNickname"),
        hint: "",
        autoCapitalize: "none",
        error: ""
      },
      {
        value: "",
        step: 2,
        name: "password",
        placeholder: localization.t("yourPassword"),
        hint: "",
        autoCapitalize: "none",
        error: ""
      }
    ]
  },

  // Selectors
  selectors: {
    isFinishStep: createSelector(
      state => state.steps,
      state => state.step,
      (steps, step) => step >= steps
    )
  },

  // Actions
  creators: duck => ({
    updateField: props => ({ type: duck.types.UPDATE_INPUT, payload: props }),
    updateStep: step => ({ type: duck.types.UPDATE_STEP, payload: step }),
    toggleLoading: state => ({ type: duck.types.LOADING, payload: state }),

    signin: fields => ({
      type: duck.types.SIGNIN,
      payload: {
        request: {
          url: "/public/sign-in",
          method: "POST",
          data: desctructFields(fields)
        }
      }
    })
  }),

  // reducer
  reducer: (state, action, duck) => {
    switch (action.type) {
      case duck.types.LOADING:
        return {
          ...state,
          loading: action.payload
        };

      case duck.types.UPDATE_INPUT:
        return {
          ...state,
          nextStep: state.step + 1,
          inputs: state.inputs.map(input =>
            input.name === action.payload.name
              ? { ...input, ...action.payload }
              : input
          )
        };

      case duck.types.UPDATE_STEP:
        return {
          ...state,
          step: action.payload,
          nextStep: false
        };

      case duck.types.SIGNIN:
        return {
          ...state,
          loading: true
        };

      case `${duck.types.SIGNIN}_FAIL`:
        return {
          ...state,
          loading: false
        };

      case `${duck.types.SIGNIN}_SUCCESS`:
        return {
          ...state,
          loading: false,
          token: action.payload.data.token
        };

      default:
        return state;
    }
  }
});
