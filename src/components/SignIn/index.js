/* eslint-disable no-shadow */
import * as React from "react";
import DropdownAlert from "react-native-dropdownalert";
import { AsyncStorage } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { withGalio } from "galio-framework";
import PropTypes from "prop-types";
import { ApplicationContext } from "context";
import withDuck from "hocs/withDuck";
import localization from "localization";
import * as validation from "validation";
import { createSummaryFields } from "services/form";
import { createStackNavigator } from "@react-navigation/stack";
import AuthenticationStep from "components/common/AuthenticationStep";

const Stack = createStackNavigator();
const SignIn = ({
  fields,
  step,
  isFinishStep,
  loading,
  nextStep,
  actions,
  theme,
  token
}) => {
  const alertRef = React.useRef();
  const navigation = useNavigation();
  const { login } = React.useContext(ApplicationContext);

  React.useEffect(() => {
    async function loginUser() {
      await AsyncStorage.setItem("@auth-token", token);
      login(token);
    }

    if (nextStep && !isFinishStep) {
      navigation.push(`Signin-step-${nextStep}`);
      actions.updateStep(nextStep);
    }

    if (token) loginUser();
  }, [navigation, step, nextStep, isFinishStep, token, login, actions]);

  const onNext = React.useCallback(
    async (name, value) => {
      try {
        const { Yup, SignInScheme } = validation;

        await Yup.reach(SignInScheme, name).validate(value);
        actions.updateField({ name, value });

        if (isFinishStep)
          await actions.signin(createSummaryFields(fields, name, value));
      } catch ({ message = "Unknown" }) {
        alertRef.current.alertWithType(
          "error",
          localization.t("error"),
          localization.t(message, { defaultValue: message })
        );

        actions.toggleLoading(false);
      }
    },
    [actions, isFinishStep, fields]
  );

  const onBack = React.useCallback(() => {
    actions.updateStep(step - 1);
    navigation.goBack();
  }, [actions, step, navigation]);

  return (
    <>
      <Stack.Navigator>
        {fields.map(field => {
          const name = `Signin-step-${field.step}`;

          return (
            <Stack.Screen
              key={name}
              name={name}
              options={{
                headerTransparent: true,
                headerTitle: "",
                headerLeft: null
              }}
            >
              {props => (
                <AuthenticationStep
                  type="signin"
                  step={field.step}
                  field={field}
                  onBack={onBack}
                  onNext={onNext}
                  loading={loading}
                  {...props}
                />
              )}
            </Stack.Screen>
          );
        })}
      </Stack.Navigator>
      <DropdownAlert
        ref={alertRef}
        messageStyle={{
          fontFamily: "lato-regular",
          color: theme.COLORS.WHITE
        }}
      />
    </>
  );
};

SignIn.propTypes = {
  actions: PropTypes.shape({
    updateField: PropTypes.func.isRequired,
    updateStep: PropTypes.func.isRequired,
    signin: PropTypes.func.isRequired,
    toggleLoading: PropTypes.func.isRequired
  }).isRequired,
  step: PropTypes.number.isRequired,
  nextStep: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]).isRequired,
  isFinishStep: PropTypes.bool.isRequired,
  theme: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  token: PropTypes.string.isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      step: PropTypes.number.isRequired
    }).isRequired
  ).isRequired
};

export const options = {
  headerTransparent: true,
  headerTitle: "",
  headerLeft: null
};

export default withDuck((state, selectors, ownState) => {
  return {
    fields: ownState.inputs,
    step: ownState.step,
    nextStep: ownState.nextStep,
    isFinishStep: selectors.isFinishStep(ownState),
    loading: ownState.loading,
    token: ownState.token
  };
})(withGalio(SignIn), "signin");
