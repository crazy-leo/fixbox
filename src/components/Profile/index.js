import React from "react";
import Wrapper from "components/common/Wrapper";
import Header from "components/common/Header";

export default () => {
  return (
    <Wrapper>
      <Header />
    </Wrapper>
  );
};

export const options = {
  headerShown: false
};
