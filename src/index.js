/* eslint-disable import/order */
import * as React from "react";
import { registerRootComponent } from "expo";
import { ThemeProvider } from "styled-components";
import { Provider } from "react-redux";
import { GalioProvider } from "galio-framework";
import { ApplicationContext } from "context";
import * as Sentry from "sentry-expo";
import env from "@env";
import { setCustomText } from "react-native-global-props";

import useApp from "hooks/useApp";
import getTheme, { getNavigationTheme } from "theme";
import store from "./store";

import Splash from "components/Splash";
import Navigator from "./navigator";

// Create sentry
Sentry.init({
  dsn: env.sentryDsn,
  enableInExpoDevelopment: false,
  debug: true
});

// Create application
const Application = () => {
  const { token, isAppReady, login } = useApp();
  const theme = getTheme();
  const navigationTheme = getNavigationTheme();

  if (isAppReady) {
    setCustomText({
      fontFamily: "lato-regular"
    });
  }

  return !isAppReady ? (
    <Splash />
  ) : (
    <ApplicationContext.Provider value={{ login }}>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <GalioProvider theme={theme}>
            <Navigator token={token} theme={navigationTheme} />
          </GalioProvider>
        </ThemeProvider>
      </Provider>
    </ApplicationContext.Provider>
  );
};

export default registerRootComponent(Application);
