import Constants from "expo-constants";

const { manifest } = Constants;
const { debuggerHost } = manifest;

export default {
  stage: {},
  prod: {},
  dev: {
    apiUrl: `http://${debuggerHost.split(":").shift()}:3000/api/v1`,
    sentryDsn: "https://b326b8828cbf467cbba9f1be56471762@sentry.io/2515821"
  }
}[manifest.release || "dev"];
